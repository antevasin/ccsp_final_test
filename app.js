/**
 * Module dependencies.
 */

var express = require('express')
  , mongoose = require('mongoose')
  , http = require('http')
  , path = require('path');
var userDBModel = require('./models/user-model.js');
var newsDBModel = require('./models/news-model.js');
var folderDBModel = require('./models/folder-model.js');
var chatDBModel = require('./models/chat-model.js');
var chatFloorDBModel = require('./models/chat-floor-model.js');
// var chatDBModel = newsDBModel.chatDBModel;

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('joiwqeurnr345p2u0fmk2lu1rn3'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

// Database Connections
var databaseUrl = process.env.FINAL_DATABASE || 'mongodb://localhost/test';
mongoose.connect(databaseUrl);

app.get('/', function(req, res){
  res.redirect('/list');
});

//clear database
app.get('/clear', function(req, res){
  userDBModel.remove(function(err){
    if(err) throw err;
    newsDBModel.remove(function(err){
      if(err) throw err;
      folderDBModel.remove(function(err){
        if(err) throw err;
        res.redirect('/list');
      });
    });
  });
});

//list all
app.get('/list', function(req, res){

  userDBModel.find().exec(function(err, docs, next){
    if(err) {
      next(err);
      return;
    }
    newsDBModel.find().exec(function(err, docs2, next){
      if(err) {
        next(err);
        return;
      }
      folderDBModel.find().exec(function(err, docs3, next){
        if(err) {
          next(err);
          return;
        }
        console.log("user:"+docs);
        console.log("news:"+docs2);
        console.log("folder:"+docs3);
        res.render('list', {users: docs, newss: docs2, folders: docs3});
      });
    });
  });
});

//user page
app.get('/user/:user_id', function(req, res){
  var id = parseInt(req.params.user_id);
  userDBModel.find({fb_id:id}).exec(function(err, docs, next){
    if(err) {
      next(err);
      return;
    }
    res.render('user', {user:docs[0]});
  });
});

//news page
app.get('/news/:news_id', function(req, res){
  var id = parseInt(req.params.news_id);
  newsDBModel.find({news_id:id}).exec(function(err, docs, next){
    if(err) {
      next(err);
      return;
    }
    var news = docs[0];
    news.click_amount++;
    news.save(function(err){
      if(err) throw err;
      folderDBModel.find({open:true}).exec(function(err, docs2, next){
        if(err){
          next(err);
          return;
        }
        res.render('news', {news:news, folders:docs2});        
      });
    });
  });
});

//folder page
app.get('/folder/:folder_id', function(req, res){
  var id = parseInt(req.params.folder_id);
  folderDBModel.find({folder_id:id}).exec(function(err, docs, next){
    if(err) {
      next(err);
      return;
    }
    res.render('folder', {folder:docs[0]});
  });
});

//register user page
app.get('/submit', function(req, res){
  res.render('submit');
})

//register
app.get('/register', function(req, res){
  var user = new userDBModel({
    fb_id : parseInt(req.query.user_id)
  });
  user.save(function(err){
    if(err) throw err;
    res.redirect('/list');
  });
});

//upload news article
app.get('/upload/:user_id', function(req, res){
  var userID = parseInt(req.params.user_id);
  var newsID = parseInt(req.query.news_id);
  var news = new newsDBModel({
    news_id : newsID,
    post_user_id : userID,
    title : req.query.news_title,
    content : req.query.news_content
  });
  if(req.query.news_author) news.author = req.query.news_author;
  if(req.query.news_resource) news.author = req.query.news_resource;
  if(req.query.news_pic) news.pic_url = req.query.news_pic;
  if(req.query.news_iframe) news.iframe_url = req.query.news_iframe;

  news.save(function(err){
    if(err) throw err;
    // console.log('save news id : ' + newsID);
    userDBModel.find({fb_id:userID} , function(err, docs, next){
      if(err) {
        next(err);
        return;
      }
      var user = docs[0];
      user.post.push(newsID);
      user.save(function(err){
        if(err) throw err;
        res.redirect('/user/'+userID);
      });
    });
  });
});

//create folder from user page
app.get('/create/user/:user_id', function(req, res){
  var userID = parseInt(req.params.user_id);
  var folderID = parseInt(req.query.folder_id);
  var folder = new folderDBModel({
    folder_id : folderID,
    folder_name : req.query.folder_name,
    user_array : [userID]
  });
  folder.save(function(err){
    if(err) throw err;
    userDBModel.find({fb_id:userID}, function(err, docs, next){
      if(err){
        next(err);
        return;
      }
      var user = docs[0];
      user.collect.push(folderID);
      user.save(function(err){
        if(err) throw err;
        res.redirect('/user/'+userID);
      });
    });
  });
});

//create folder from news page
app.get('/create/news/:news_id', function(req, res){
  var userID = parseInt(req.query.user_id);
  var newsID = parseInt(req.params.news_id);
  var folderID = parseInt(req.query.folder_id);
  var folder = new folderDBModel({
    folder_id : folderID,
    folder_name : req.query.folder_name,
    user_array : [userID],
    news_array : [newsID]
  });
  folder.save(function(err){
    if(err) throw err;
    userDBModel.find({fb_id:userID}, function(err, docs, next){
      if(err){
        next(err);
        return;
      }
      var user = docs[0];
      user.collect.push(folderID);
      user.save(function(err){
        if(err) throw err;
        newsDBModel.find({news_id:newsID}, function(err, docs2, next){
          if(err){
            next(err);
            return;
          }
          var news = docs2[0];
          news.folder_array.push(folderID);
          news.save(function(err){
            if(err) throw err;
            res.redirect('/news/'+newsID);
          });
        });
      });
    });
  });
});

//add news to folder
app.get('/add/:folder_id/:news_id', function(req, res){
  var folderID = parseInt(req.params.folder_id);
  var newsID = parseInt(req.params.news_id);
  folderDBModel.find({folder_id:folderID}, function(err, docs, next){
    if(err) {
      next(err);
      return;
    }
    var folder = docs[0];
    folder.news_array.push(newsID);
    folder.save(function(err){
      if(err) throw err;
      newsDBModel.find({news_id:newsID}, function(err, docs2, next){
        if(err) {
          next(err);
          return;
        }
        var news = docs2[0];
        news.folder_array.push(folderID);
        news.save(function(err){
          if(err) throw err;
          res.redirect('/news/'+newsID);
        });
      });
    });
  });
});

app.get('/folder/change/:folder_id', function(req, res){
  var folderID = parseInt(req.params.folder_id);
  folderDBModel.find({folder_id:folderID}, function(err, docs, next){
    if(err) {
      next(err);
      return;
    }
    var folder = docs[0];
    folder.open = !folder.open;
    folder.save(function(err){
      if(err) throw err;      
      res.redirect('/folder/'+folderID);
    });
  });
});


//testt news chat room
app.get('/view', function(req, res){
  res.render('view');
})

app.post('/news/:news_id/:floor', function(req, res){
  var newsID = parseInt(req.params.news_id);
  var floor = parseInt(req.params.floor)-1;
  if(isNaN(floor)) floor = -1;
  var userID = req.session.fbid || 1;
  var reply = req.body.reply;
  console.log(newsID+"     "+floor+"   "+"       "+userID+" "+reply);
  if(!reply){
    res.end('');
  }
  newsDBModel.find({'news_id':newsID},function(err, doc, next){
    if(err){
      next(err);
      return;
    }
    var news = doc[0];
    var chat = new chatDBModel({
      'user_id' : userID,
      'talk' : reply
    });
    if(floor == -1) {
      var floor = new chatFloorDBModel({ 
        'chat_floor' : [chat]
      });
      news.chat_room.push(floor);
    }
    else if(floor >= 0) {
      news.chat_room[floor].chat_floor.push(chat);
    }
    console.log(news.chat_room);
    news.save(function(err){
      if(err) throw err;
      res.end("");
    });
  });
});


http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
