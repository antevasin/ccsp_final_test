(function($){
  "use strict";

  var

  // Commonly used jQuery objects.
  $mainList = $('.main'),
  $placeholder = $('#placeholder'),

  // Helper function that generates <li> element.
  todoFactory = (function(){

    // cached element
    var $el = $('<li><input type="text" placeholder="New task..."><span class="text"></span></li>');

    // This returned function will be stored in 'todoFactory' variable.
    return function(){
      // Return a new clone of the cached element.
      return $el.clone();
    }
  }()),

  // Save to local storage.
  save = function(){
    $placeholder.addClass('loading');
    var todos = $('.main li').map(function(){
      var $li = $(this)
      return {text: $li.find('.text').text(), done: $li.hasClass('done')};
    }).get();

    // TODO :
    // Pass the 'todos' variable to backend via ajax.
    $.post('/add', {data: todos}, function(result){
        //alert( "Data Saved: " + msg );
        console.log("post");
    });
        $placeholder.removeClass('loading');
    // $.ajax({
    //   url: "/add",
    //   type: "POST",
    //   data: todos,
    //   complete: function() {
    //     //called when complete
    //     console.log('process complete');
    //     $placeholder.removeClass('loading');
    //   },

    //   success: function(data) {
    //     console.log('process sucess');
    //     $placeholder.removeClass('loading');
    //   },

    //   error: function() {
    //     console.log('process error');
    //     $placeholder.removeClass('loading');
    //   },
    // });
  };

  // Initialize sortable on the three <ul>s.
  $('.connected').sortable({
    connectWith: '.connected',
    tolerance: 'pointer', // better collision detection
    items: 'li'           // not mandatory
  });

  // Add new <li>.
  $('#add').click(function(){
    var $li = todoFactory().addClass('editing')
                           .prependTo($mainList);
    $li.find('input').focus();
  });

  // Finish editing <li>.
  $mainList.on('blur', 'li', function(){
    var $li = $(this), $input = $li.find('input');

    // Hidden inputs sometimes triggers blur events......
    if(!$li.hasClass('editing')) return;

    // Remove class 'editing' also makes sure the following code
    // is executed only once (because the if statement above.)
    $li.removeClass('editing')
       .find('span').text( $.trim( $input.val() ) );

    if($input.val()){
      save();
    }else{
      $li.remove();
    }
  }).on('keypress', 'li', function(e){
    if(e.which === 13){
      $(e.target).blur();
    }
  }).on('sortstart', function(){
    // Show .delete and .done .
    $placeholder.addClass('dragging');
  }).on('sortstop', function(){
    // Hide .delete and .done .
    $placeholder.removeClass('dragging');
    // Save to localStorage only when changes are made.
    save();
  });

  // Set as done.
  $('.done').on('sortreceive', function(e, ui){
    var $li = ui.item;
    $li.addClass('done').appendTo($mainList);
  });

  // Remove item.
  $('.delete').on('sortreceive', function(e, ui){
    var $li = ui.item;
    $li.remove();
  });

  // Spin.js
  var spinner = new Spinner({
    lines: 9, // The number of lines to draw
    length: 3, // The length of each line
    width: 3, // The line thickness
    radius: 4, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    color: '#fff', // #rgb or #rrggbb
    speed: 1.3, // Rounds per second
    trail: 52, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: true, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: '5px', // Top position relative to parent in px
    left: '5px' // Left position relative to parent in px
  }).spin($('.spin').get(0));

}(jQuery));
