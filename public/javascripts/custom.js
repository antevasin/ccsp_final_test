$(document).ready(function(){
    $('#hot_div').hide();
    $('#hot_div').css("visibility","hidden");
    $('#post').hide();
    $('#friend').hide();

    $('#latest_btn').click(function(){
        $('#latest_div').css("visibility","visible");
        $('#latest_div').show();
        $('#hot_div').css("visibility","hidden");
        $('#hot_div').hide();
    });

    $('#hot_btn').click(function(){
        $('#hot_div').css("visibility","visible");
        $('#hot_div').show();
        $('#latest_div').css("visibility","hidden");
        $('#latest_div').hide();
    });
    $('.tile').mouseover(function(){
        $('this').addClass('selected');
    });
    $('.tile').mouseout(function(){
        $('this').removeClass('selected');
    });

    $('#collection_btn').click(function(){
        $('#collection').show();
        $('#post').hide();
        $('#friend').hide();
    });
    $('#mypost_btn').click(function(){
        $('#post').show();
        $('#collection').hide();
        $('#friend').hide();
    });
    $('#myfriend_btn').click(function(){
        $('#post').hide();
        $('#collection').hide();
        $('#friend').show();
    });

});