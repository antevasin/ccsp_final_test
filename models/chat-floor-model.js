var mongoose = require('mongoose');
var chatDBModel = require('./chat-model.js');
// var chat = new mongoose.Schema({
// 	user_id : Number
// 	, talk : String
// 	, time : {type : Date, default : new Date()}
// });
var chatFloorDBSchema = new mongoose.Schema({
	chat_floor : [chatDBModel]
});
// var newsDBSchema = new mongoose.Schema({
// 	news_id : {type : Number, unique : true, require : true}
// 	, post_user_id : {type : Number, require : true}
// 	, title : {type : String, require : true}
// 	, content : {type : String, require : true}
// 	, author : {type : String, default : "someone", require : true}
// 	, resource : {type : String, default : "someplace", require : true}
// 	, news_time : {type : String, default : "sometime", require : true }
// 	, post_time : {type : Date, default : Date.now, require : true }
// 	, pic_url : {type : String, default : "", require : true}
// 	, iframe_url : {type : String, default : "", require : true}

// 	, click_amount : {type : Number, default : 0, require : true}
// 	//, post : {type : [Number], default : [], require : true}
// 	//, collect : {type : [Number], default : [], require : true}
// 	, folder_array : {type : [Number], default : [], require : true}
// 	, chat_room : {type : [chatFloor], default : [[]], require : true}
// });
module.exports = mongoose.model('chatFloorDBModel', chatFloorDBSchema);

// exports.chatDBModel = mongoose.model('ChatDBModel', chatDBModel);