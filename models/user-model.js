// TODO: Define your own models!
var mongoose = require('mongoose');
var userDBSchema = new mongoose.Schema({
	fb_id : {type : Number, unique : true, require : true}
	, post : {type : [Number], default : [], require : true}
	, collect : {type : [Number], default : [], require : true}
});
module.exports = mongoose.model('UserDBModel', userDBSchema);