var mongoose = require('mongoose');
var folderDBSchema = new mongoose.Schema({
	folder_id : {type : Number, unique : true, require : true}
	, folder_name : {type : String, require : true}
	, folder_time : {type : Date, default : Date.now, require : true }
	, open : {type : Boolean, default : false, require : true}
	, user_array : {type : [Number], default : [], require : true}
	, news_array : {type : [Number], default : [], require : true}
});
module.exports = mongoose.model('FolderDBModel', folderDBSchema);